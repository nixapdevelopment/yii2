<?php

return [
    'adminEmail' => 'admin@example.com',
    'lang' => [
        'ru' => [
            'name' => 'Russian',
        ],
        'en' => [
            'name' => 'English',
        ],
        'ro' => [
            'name' => 'Romanian',
        ],
    ],
    'dateTimeFormat' => 'd.m.Y H:i',
    'dateTimeFormatJs' => 'dd.mm.yyyy hh:ii',
];
