<?php
namespace app\controllers;

use app\modules\Category\models\Category;
use app\modules\Post\models\Post;
use yii\data\ActiveDataProvider;

class FBlogController extends \yii\web\Controller
{
    
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Category::find()->with('lang'),
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCategory($id)
    {
        $category = Category::find()->with(['lang'])->where(['ID' => $id])->one();
        
        $dataProvider = new ActiveDataProvider([
            'query' => Post::find()->with(['lang', 'comments'])->where(['CategoryID' => $category->ID]),
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);
        
        return $this->render('category', [
            'category' => $category, 
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPost($id)
    {
        $post = Post::find()->with('lang')->where(['ID' => $id])->one();
        
        return $this->render('post', [
            'post' => $post,
        ]);
    }

}