<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Coment".
 *
 * @property integer $ID
 * @property integer $ArticleID
 * @property string $Coment
 *
 * @property Article $article
 */
class Coment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Coment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ArticleID', 'Coment'], 'required'],
            [['ArticleID'], 'integer'],
            [['Coment'], 'string'],
            [['ArticleID'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['ArticleID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ArticleID' => Yii::t('app', 'Article ID'),
            'Coment' => Yii::t('app', 'Coment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['ID' => 'ArticleID']);
    }
}
