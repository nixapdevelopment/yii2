<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Article".
 *
 * @property integer $ID
 * @property string $Title
 * @property string $Content
 * @property string $CreatedAt
 */
class ArticleMain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Title', 'Content'], 'required'],
            [['Content'], 'string'],
            [['CreatedAt'], 'safe'],
            [['Title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Title' => Yii::t('app', 'Title'),
            'Content' => Yii::t('app', 'Content'),
            'CreatedAt' => Yii::t('app', 'Created At'),
        ];
    }
}
