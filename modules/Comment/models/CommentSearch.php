<?php

namespace app\modules\Comment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Comment\models\Comment;
use yii\db\Expression;

/**
 * CommentSearch represents the model behind the search form about `app\modules\Comment\models\Comment`.
 */
class CommentSearch extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'PostID'], 'integer'],
            [['Content', 'CreatedAt', 'Status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find()->with(['post.lang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if ($this->CreatedAt)
        {
            $query->andFilterWhere(['between', 'CreatedAt', date('Y-m-d 00:00:00', strtotime($this->CreatedAt)), date('Y-m-d 23:59:59', strtotime($this->CreatedAt))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PostID' => $this->PostID,
        ]);

        $query->andFilterWhere(['like', 'Content', $this->Content])
            ->andFilterWhere(['like', 'Status', $this->Status]);

        return $dataProvider;
    }
}
