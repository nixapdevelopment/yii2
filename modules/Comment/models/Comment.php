<?php

namespace app\modules\Comment\models;

use Yii;
use app\modules\Post\models\Post;

/**
 * This is the model class for table "Comment".
 *
 * @property integer $ID
 * @property integer $PostID
 * @property string $Content
 * @property string $CreatedAt
 * @property string $Status
 *
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord
{
    
    const StatusDenied = 'Denied';
    const StatusApproved = 'Approved';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PostID', 'Content', 'Status'], 'required'],
            [['PostID'], 'integer'],
            [['Content'], 'string'],
            [['CreatedAt'], 'safe'],
            [['Status'], 'string', 'max' => 50],
            [['PostID'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['PostID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PostID' => Yii::t('app', 'Post ID'),
            'Content' => Yii::t('app', 'Content'),
            'CreatedAt' => Yii::t('app', 'Created At'),
            'Status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['ID' => 'PostID']);
    }
    
    public function getNiceDate()
    {
        return date(Yii::$app->params['dateTimeFormat'], strtotime($this->CreatedAt));
    }
    
    public static function getStatusList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        
        $list = [
            self::StatusApproved => Yii::t('app', 'Status approved'),
            self::StatusDenied => Yii::t('app', 'Status denied'),
        ];
        
        return $return + $list;
    }
    
}
