<?php

namespace app\modules\Comment\widgets\CommentWidget;

use yii\base\Widget;
use yii\base\InvalidConfigException;
use app\modules\Comment\models\Comment;

class CommentWidget extends Widget
{
    
    public $postID = null;

    public function init()
    {
        parent::init();
        
        if (!$this->postID)
        {
            throw new InvalidConfigException('Option postID is required');
        }
    }

    public function run()
    {
        $comments = Comment::find()->where(['PostID' => $this->postID, 'Status' => Comment::StatusApproved])->all();
        
        return $this->render('index', [
            'comments' => $comments,
        ]);
    }
    
}
