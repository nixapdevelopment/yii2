<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Comment\models\Comment;

/* @var $this yii\web\View */
/* @var $model app\modules\Comment\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::tag('h2', $model->post->lang->Title) ?>

    <?= $form->field($model, 'Content')->textarea(['rows' => 6]) ?>

    <?= Html::tag('h2', $model->niceDate) ?>

    <?= $form->field($model, 'Status')->dropDownList(Comment::getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
