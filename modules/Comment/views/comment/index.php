<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use app\modules\Comment\models\Comment;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Comment\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\modules\Comment\models\Comment */

$this->title = Yii::t('app', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Comment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'post.lang.Title',
                    'label' => 'Post',
                ],
                'Content:ntext',
                [
                    'attribute' => 'CreatedAt',
                    'value' => function($model)
                    {
                        return $model->niceDate;
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel, 
                        'attribute' => 'CreatedAt',
                        'options' => ['placeholder' => 'Enter date'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]),
                ],
                [
                    'attribute' => 'Status',
                    'filter' => Html::activeDropDownList($searchModel, 'Status', Comment::getStatusList(Yii::t('app', 'Select status')), ['class' => 'form-control']),
                ],

                [
                    'class' => 'yii\grid\ActionColumn'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
