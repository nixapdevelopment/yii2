<?php

namespace app\modules\Category\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\Post\models\Post;

/**
 * This is the model class for table "Category".
 *
 * @property integer $ID
 * @property string $CreatedAt
 * @property integer $ParentID
 *
 * @property CategoryLang[] $langs
 *  @property CategoryLang  $lang
 * @property Post[] $posts
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CreatedAt'], 'safe'],
            [['ParentID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'CreatedAt' => Yii::t('app', 'Created At'),
            'ParentID' => Yii::t('app', 'Parent ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(CategoryLang::className(), ['CategoryID' => 'ID'])->indexBy('LangID')->all();
        $result = [];
        foreach (array_keys(Yii::$app->params['lang']) as $LangID){
            $result[$LangID]= isset($langs[$LangID]) ? $langs[$LangID] : new CategoryLang([
                'LangID' => $LangID,
            ]);
        }
        return $result;
    }

    public function getLang()
    {
        return $this->hasOne(CategoryLang::className(), ['CategoryID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['CategoryID' => 'ID'])->orderBy('CreatedAt DESC');
    }
    
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
    
}
