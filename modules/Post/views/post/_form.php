<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Category\models\Category;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;
use app\modules\Tag\models\Tag;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\Post\models\Post */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CategoryID')->dropDownList(Category::getList(Yii::t('app', 'Select category'))) ?>

    <?= $form->field($model, 'CreatedAt')->widget(DateTimePicker::className(), [
        'options' => [
            'value' => $model->niceDate,
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => Yii::$app->params['dateTimeFormatJs'],
	]
    ]) ?>
    
    <?php
        $items = [];
        foreach ($model->langs as $langID => $langModel)
        {
            $items[] =  [
                'label' => strtoupper($langID),
                'content' => $this->render('_lang_form', [
                    'form' => $form,
                    'langModel' => $langModel,
                ]),
                'active' => $langID === reset(Yii::$app->params['lang']),
            ];
        }
    ?>
    
    <?= Tabs::widget([
        'items' => $items,
    ]) ?>
    
    <?= $form->field($model, 'TagIDs[]')->widget(Select2::className(), [
        'data' => Tag::getList(),
        'options' => [
            'placeholder' => 'Select a color ...', 
            'multiple' => true,
            'value' => $model->tagIDs,
        ],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10,
            'createTag' => JsExpression('function (tag) {
                return {
                    id: tag.term,
                    text: tag.term,
                    isNew : true
                };
            }'),
        ],
        'pluginEvents' => [
            'select2:select' => JsExpression('function (e) {
                var select = $(this);
                if(e.params.data.isNew){
                    $.get("' . Url::to(['/blog/tag/tag/add-tag']) . '", {title: e.params.data.id}, function(json){
                        select.find(\'[value="\'+e.params.data.id+\'"]\').replaceWith(\'<option selected value="\'+ json.ID +\'">\'+e.params.data.text+\'</option>\');
                    }, "json");
                }
            }'),
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>