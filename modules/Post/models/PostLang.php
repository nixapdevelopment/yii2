<?php

namespace app\modules\Post\models;

use Yii;

/**
 * This is the model class for table "PostLang".
 *
 * @property integer $ID
 * @property integer $PostID
 * @property string $LangID
 * @property string $Title
 * @property string $Content
 *
 * @property Article $article
 */
class PostLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PostLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PostID', 'LangID', 'Title', 'Content'], 'required'],
            [['PostID'], 'integer'],
            [['Content'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['PostID'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['PostID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PostID' => Yii::t('app', 'Article ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['ID' => 'PostID']);
    }
}
