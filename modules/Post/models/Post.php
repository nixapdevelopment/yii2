<?php

namespace app\modules\Post\models;


use Yii;
use app\modules\Category\models\Category;
use app\modules\Comment\models\Comment;
use yii\base\Model;
use app\modules\Tag\models\PostTag;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Post".
 *
 * @property integer $ID
 * @property integer $CategoryID
 * @property string $CreatedAt
 * @property string $niceDate
 *
 * @property Category $category
 * @property PostLang[] $langs
 */
class Post extends \yii\db\ActiveRecord
{
    
    public $TagIDs = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CategoryID'], 'required'],
            [['CategoryID'], 'integer'],
            [['CreatedAt'], 'required', 'on' => 'create'],
            [['CreatedAt', 'TagIDs'], 'safe'],
            [['CategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['CategoryID' => 'ID'], 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'CategoryID' => Yii::t('app', 'Category ID'),
            'CreatedAt' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['ID' => 'CategoryID']);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['PostID' => 'ID']);
    }
    
    public function getLang()
    {
        return $this->hasOne(PostLang::className(), ['PostID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }
    
    public function getLangs()
    {
        $langs = $this->hasMany(PostLang::className(), ['PostID' => 'ID'])->indexBy('LangID')->all();
        $result = [];
        foreach (array_keys(Yii::$app->params['lang']) as $LangID){
            $result[$LangID]= isset($langs[$LangID]) ? $langs[$LangID] : new PostLang([
                'LangID' => $LangID,
            ]);
        }
        return $result;
    }
    
    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['PostID' => 'ID']);
    }
    
    public function getTags()
    {
        return $this->hasMany(PostTag::className(), ['PostID' => 'ID'])->with('tag');
    }
    
    public function getNiceDate()
    {
        return date(Yii::$app->params['dateTimeFormat'], strtotime($this->CreatedAt));
    }
    
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        
        $this->CreatedAt = date('Y-m-d H:i:s', strtotime($this->CreatedAt));
        
        return true;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $langModels = $this->langs;
        foreach ($langModels as $langModel)
        {
            $langModel->PostID = $this->ID;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
        
        PostTag::deleteAll(['PostID' => $this->ID]);
        foreach ((array)$this->TagIDs as $tagID)
        {
            $postTag = new PostTag([
                'PostID' => $this->ID,
                'TagID' => $tagID,
            ]);
            $postTag->save();
        }
    }
    
    public function getTagIDs()
    {
        return ArrayHelper::map($this->postTags, 'TagID', 'TagID');
    }
    
    public function getShortContent()
    {
        return mb_substr(strip_tags($this->lang->Content), 0, 50) . ' ' . \yii\bootstrap\Html::a('More', ['/f-blog/post', 'id' => $this->ID]);
    }
    
}
