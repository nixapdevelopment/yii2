<?php

namespace app\modules\Tag\models;

use Yii;
use app\modules\Tag\models\PostTag;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Tag".
 *
 * @property integer $ID
 * @property string $Title
 *
 * @property PostTag[] $postTags
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Title'], 'required'],
            [['Title'], 'string', 'max' => 255],
            [['Title'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['TagID' => 'ID']);
    }
    
    
    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'ID', 'Title');
    }
    
}
