<?php

namespace app\modules\Tag\models;

use Yii;
use app\modules\Post\models\Post;

/**
 * This is the model class for table "PostTag".
 *
 * @property integer $ID
 * @property integer $PostID
 * @property integer $TagID
 *
 * @property Post $post
 * @property Tag $tag
 */
class PostTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PostTag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PostID', 'TagID'], 'integer'],
            [['PostID'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['PostID' => 'ID']],
            [['TagID'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::className(), 'targetAttribute' => ['TagID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PostID' => Yii::t('app', 'Post ID'),
            'TagID' => Yii::t('app', 'Tag ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['ID' => 'PostID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['ID' => 'TagID']);
    }
}
