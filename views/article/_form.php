<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Content')->widget(TinyMce::className(),[]) ?>

    <?= $form->field($model, 'CreatedAt')->widget(DatePicker::className(),[
            'options' =>['value' => $model->niceDate,],
            'pluginOptions' => [
        'format' => 'dd.mm.yyyy',
        'todayHighlight' => true
    ]]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php print_r($model->coments);?>
    <?php ActiveForm::end(); ?>

</div>

<?php

$provider = new ArrayDataProvider([
    'allModels' => $model->coments,
    'pagination' => [
        'pageSize' => 10,
    ],
]);
echo GridView::widget([
'dataProvider' => $provider,
]);