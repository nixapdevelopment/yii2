<?php

use yii\bootstrap\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
?>

<?= Html::tag('h1', $category->lang->Title) ?>

<?php \yii\widgets\Pjax::begin() ?>

    <div class="row">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'layout' => "{items}<div class=\"clearfix\"></div>{pager}"
        ]); ?>
    </div>

<?php \yii\widgets\Pjax::end() ?>