<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
?>

<?php \yii\widgets\Pjax::begin() ?>

    <div class="row">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_category_item',
        ]); ?>
    </div>

<?php \yii\widgets\Pjax::end() ?>
