<?php

    use yii\bootstrap\Html;
    use app\modules\Comment\widgets\CommentWidget\CommentWidget;

?>


    <div>
        <?= $post->lang->Title ?>
    </div>
    <div>
        <?= $post->niceDate ?>
    </div>
    <div>
        <?= $post->lang->Content ?>
    </div>
    <div>
        <?= CommentWidget::widget([
            'postID' => $post->ID,
        ]) ?>
    </div>