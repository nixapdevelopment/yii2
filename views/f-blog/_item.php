<?php

use yii\bootstrap\Html;

?>

<div class="col-md-4">
    <div>
        <?= Html::a($model->lang->Title, ['/f-blog/post', 'id' => $model->ID]) ?>
    </div>
    <div>
        <?= $model->niceDate ?>
    </div>
    <div>
        <?= $model->shortContent ?>
    </div>
    <div>
        Comments: <?= count($model->comments) ?>
    </div>
    <hr />
</div>