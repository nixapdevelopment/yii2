-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.53 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица test.Category
CREATE TABLE IF NOT EXISTS `Category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ParentID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.Category: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` (`ID`, `CreatedAt`, `ParentID`) VALUES
	(1, '2017-07-12 20:33:08', 0),
	(2, '2017-07-26 18:30:45', NULL);
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;

-- Дамп структуры для таблица test.CategoryLang
CREATE TABLE IF NOT EXISTS `CategoryLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CategoryLang` (`CategoryID`),
  KEY `LangID` (`LangID`),
  CONSTRAINT `CategoryLang` FOREIGN KEY (`CategoryID`) REFERENCES `Category` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.CategoryLang: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `CategoryLang` DISABLE KEYS */;
INSERT INTO `CategoryLang` (`ID`, `CategoryID`, `LangID`, `Title`) VALUES
	(1, 1, 'ru', 'RUSSS'),
	(2, 2, 'ru', 'RUS 2');
/*!40000 ALTER TABLE `CategoryLang` ENABLE KEYS */;

-- Дамп структуры для таблица test.Comment
CREATE TABLE IF NOT EXISTS `Comment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostID` int(11) NOT NULL,
  `Content` text NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PostID` (`PostID`),
  CONSTRAINT `PostID` FOREIGN KEY (`PostID`) REFERENCES `Post` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.Comment: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `Comment` DISABLE KEYS */;
INSERT INTO `Comment` (`ID`, `PostID`, `Content`, `CreatedAt`, `Status`) VALUES
	(3, 2, 'asdasdasdasda dasd asdas das ', '2017-07-22 17:27:13', 'Denied'),
	(4, 1, 'asssssdasd asdas das ', '2017-07-22 17:29:58', 'Approved');
/*!40000 ALTER TABLE `Comment` ENABLE KEYS */;

-- Дамп структуры для таблица test.Post
CREATE TABLE IF NOT EXISTS `Post` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(11) DEFAULT NULL,
  `CreatedAt` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CategoryID` (`CategoryID`),
  CONSTRAINT `CategoryID` FOREIGN KEY (`CategoryID`) REFERENCES `Category` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.Post: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `Post` DISABLE KEYS */;
INSERT INTO `Post` (`ID`, `CategoryID`, `CreatedAt`) VALUES
	(1, 1, '2017-07-23 14:50:00'),
	(2, 1, '2017-07-29 09:50:00');
/*!40000 ALTER TABLE `Post` ENABLE KEYS */;

-- Дамп структуры для таблица test.PostLang
CREATE TABLE IF NOT EXISTS `PostLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Content` longtext NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ArticleID` (`PostID`),
  KEY `LangID` (`LangID`),
  CONSTRAINT `FK_PostLang_Post` FOREIGN KEY (`PostID`) REFERENCES `Post` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.PostLang: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `PostLang` DISABLE KEYS */;
INSERT INTO `PostLang` (`ID`, `PostID`, `LangID`, `Title`, `Content`) VALUES
	(1, 1, 'ru', 'Test title', 'vcbcvb'),
	(2, 1, 'en', 'bcvb', 'cvbcvbc'),
	(3, 1, 'ro', 'cvbcvb', 'cvbcvb'),
	(4, 2, 'ru', 'fbhdfhdf', 'hdfhdf'),
	(5, 2, 'en', 'dfhdfh', 'dfhdfh'),
	(6, 2, 'ro', 'dfhdfhd', 'fhdfhdfh');
/*!40000 ALTER TABLE `PostLang` ENABLE KEYS */;

-- Дамп структуры для таблица test.PostTag
CREATE TABLE IF NOT EXISTS `PostTag` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostID` int(11) DEFAULT NULL,
  `TagID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_PostTag_Post` (`PostID`),
  KEY `FK_PostTag_Tag` (`TagID`),
  CONSTRAINT `FK_PostTag_Post` FOREIGN KEY (`PostID`) REFERENCES `Post` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PostTag_Tag` FOREIGN KEY (`TagID`) REFERENCES `Tag` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.PostTag: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `PostTag` DISABLE KEYS */;
INSERT INTO `PostTag` (`ID`, `PostID`, `TagID`) VALUES
	(33, 1, 2),
	(34, 1, 24);
/*!40000 ALTER TABLE `PostTag` ENABLE KEYS */;

-- Дамп структуры для таблица test.Tag
CREATE TABLE IF NOT EXISTS `Tag` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.Tag: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `Tag` DISABLE KEYS */;
INSERT INTO `Tag` (`ID`, `Title`) VALUES
	(2, 'test 1'),
	(3, 'test 2'),
	(24, 'new'),
	(25, 'gfhdfhdfh');
/*!40000 ALTER TABLE `Tag` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
